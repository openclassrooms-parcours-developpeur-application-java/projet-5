/**** OpenClassrooms - Projet 5 ****/
/* Concevez la solution technique d’un système de gestion de pizzeria */
       
       
INSERT INTO public.compte
    (nom, prenom, mot_de_passe, mail, telephone)
VALUES
    ('Bonhomme', 'Romain', 'romainbonhomme', 'romainbonhomme@fakeemail.tld', '06.46.11.42.92'),
    ('Delage', 'Tristan', 'tristandelage', 'tristandelage@fakeemail.tld', '06.72.38.01.17'),
    ('Henry', 'Éléna', 'elenahenry', 'elenahenry@fakeemail.tld', '06.79.22.90.75'),
    ('Foucher', 'Anthony', 'anthonyfoucher', 'anthonyfoucher@fakeemail.tld', '06.14.23.58.71'),
    ('Leroy', 'Émile', 'emileleroy', 'emileleroy@fakeemail.tld', '06.57.59.83.62'),
    ('Pruvost', 'Amandine', 'amandinepruvost', 'amandinepruvost@fakeemail.tld', '06.27.94.29.93'),
    ('Marion', 'Mattéo', 'matteomarion', 'matteomarion@fakeemail.tld', '06.96.19.18.05'),
    ('David', 'Théo', 'theodavid', 'theodavid@fakeemail.tld', '06.34.45.96.69'),
    ('Pages', 'Thibault', 'thibaultpages', 'thibaultpages@fakeemail.tld', '06.94.22.84.74'),
    ('Dupuis', 'Maëlle', 'maelledupuis', 'maelledupuis@fakeemail.tld', '06.83.39.99.83'),
    ('Leduc', 'Loane', 'loaneleduc', 'loaneleduc@fakeemail.tld', '06.09.38.30.92'),
    ('Gallet', 'Solene', 'solenegallet', 'solenegallet@fakeemail.tld', '06.73.08.34.76'),
    ('Jung', 'Dylan', 'dylanjung', 'dylanjung@fakeemail.tld', '06.18.62.83.83'),
    ('Didier', 'Angelina', 'angelinadidier', 'angelinadidier@fakeemail.tld', '06.79.33.49.52'),
    ('Granier', 'Thomas', 'thomasgranier', 'thomasgranier@fakeemail.tld', '06.65.01.19.90'),
    ('Raynaud', 'Gilbert', 'gilbertraynaud', 'gilbertraynaud@fakeemail.tld', '06.84.70.22.94'),
    ('Giraud', 'Yüna', 'yunagiraud', 'yunagiraud@fakeemail.tld', '06.84.73.25.08'),
    ('Leonard', 'Nicolas', 'nicolasleonard', 'nicolasleonard@fakeemail.tld', '06.32.02.21.81'),
    ('Serre', 'Clémence', 'clemenceserre', 'clemenceserre@fakeemail.tld', '06.54.80.58.73'),
    ('Lafon', 'Ambre', 'ambrelafon', 'ambrelafon@fakeemail.tld', '06.01.23.43.26'),
    ('Langlois', 'Aaron', 'aaronlanglois', 'aaronlanglois@fakeemail.tld', '06.04.92.36.92'),
    ('Clement', 'Alicia', 'aliciaclement', 'aliciaclement@fakeemail.tld', '06.30.68.54.09'),
    ('Boyer', 'Maïwenn', 'maiwennboyer', 'maiwennboyer@fakeemail.tld', '06.91.62.68.23'),
    ('Thierry', 'Yasmine', 'yasminethierry', 'yasminethierry@fakeemail.tld', '06.79.28.82.86'),
    ('Fabre', 'Florentin', 'florentinfabre', 'florentinfabre@fakeemail.tld', '06.14.79.81.45'),
    ('Vallee', 'Thomas', 'thomasvallee', 'thomasvallee@fakeemail.tld', '06.57.79.21.79'),
    ('Renard', 'Antonin', 'antoninrenard', 'antoninrenard@fakeemail.tld', '06.52.07.01.53'),
    ('Gregoire', 'Léo', 'leogregoire', 'leogregoire@fakeemail.tld', '06.65.11.11.59'),
    ('Maillet', 'Davy', 'davymaillet', 'davymaillet@fakeemail.tld', '06.08.56.64.61'),
    ('Baron', 'Élise', 'elisebaron', 'elisebaron@fakeemail.tld', '06.47.47.77.66'),
    ('Flament', 'Lucie', 'lucieflament', 'lucieflament@fakeemail.tld', '06.87.65.70.56'),
    ('Bouchet', 'Lena', 'lenabouchet', 'lenabouchet@fakeemail.tld', '06.53.81.10.24'),
    ('Marquet', 'Marine', 'marinemarquet', 'marinemarquet@fakeemail.tld', '06.06.24.17.32'),
    ('Gauthier', 'Tatiana', 'tatianagauthier', 'tatianagauthier@fakeemail.tld', '06.66.15.72.12'),
    ('Lesage', 'Nicolas', 'nicolaslesage', 'nicolaslesage@fakeemail.tld', '06.13.53.99.11'),
    ('Leblond', 'Valentin', 'valentinleblond', 'valentinleblond@fakeemail.tld', '06.67.43.94.27'),
    ('Charbonnier', 'Simon', 'simoncharbonnier', 'simoncharbonnier@fakeemail.tld', '06.68.14.01.40'),
    ('Lenoir', 'Clémence', 'clemencelenoir', 'clemencelenoir@fakeemail.tld', '06.23.19.31.87'),
    ('Duhamel', 'Carla', 'carladuhamel', 'carladuhamel@fakeemail.tld', '06.06.26.77.15'),
    ('Muller', 'Esteban', 'estebanmuller', 'estebanmuller@fakeemail.tld', '06.34.20.38.60'),
    ('Robin', 'Justine', 'justinerobin', 'justinerobin@fakeemail.tld', '06.27.06.21.69'),
    ('Boyer', 'Pauline', 'paulineboyer', 'paulineboyer@fakeemail.tld', '06.81.25.41.38'),
    ('Schneider', 'Kylian', 'kylianschneider', 'kylianschneider@fakeemail.tld', '06.85.43.73.14'),
    ('Besse', 'Pauline', 'paulinebesse', 'paulinebesse@fakeemail.tld', '06.37.06.48.10'),
    ('Herve', 'Françoise', 'francoiseherve', 'francoiseherve@fakeemail.tld', '06.75.04.95.59'),
    ('Gallet', 'Clotilde', 'clotildegallet', 'clotildegallet@fakeemail.tld', '06.52.73.24.62'),
    ('Vallet', 'Lilian', 'lilianvallet', 'lilianvallet@fakeemail.tld', '06.49.35.16.84'),
    ('Sanchez', 'Valentine', 'valentinesanchez', 'valentinesanchez@fakeemail.tld', '06.56.41.21.61'),
    ('Colas', 'Mathis', 'mathiscolas', 'mathiscolas@fakeemail.tld', '06.95.92.07.57')
;


INSERT INTO public.adresse
    (numero, rue, code_postale, localite)
VALUES
    (95, 'Rue du Dessous-des-Berges', 20130, 'Cargèse'),
    (66, 'Rue Héliopolis', 20111, 'Casaglione'),
    (57, 'Rue Gustave-Doré', 55140, 'Vaucouleurs'),
    (7, 'Rue Galleron', 75008, 'Paris 8ème'),
    (57, 'Passage Depaquit', 91890, 'Videlles'),
    (71, 'Passage Dubuffet', 16200, 'Triac-Lautrait'),
    (44, 'Rue de Belloy', 12490, 'Viala-du-Tarn'),
    (13, 'Passage Depaquit', 59146, 'Pecquencourt'),
    (54, 'Villa Garnier', 85500, 'Saint-Paul-en-Pareds'),
    (22, 'Avenue Barbey-Aurevilly', 66200, 'Théza'),
    (54, 'Impasse des Deux-Néthes', 77690, 'Montigny-sur-Loing'),
    (98, 'Rue Danton', 12490, 'Viala-du-Tarn'),
    (38, 'Rue Deguerry', 82110, 'Tréjouls'),
    (46, 'Rue de Chambéry', 59280, 'Bois-Grenier'),
    (87, 'Avenue du Général-Dodds', 80650, 'Vignacourt'),
    (17, 'Place Jacques-Rueff', 02680, 'Grugies'),
    (55, 'Rue Bayen', 91330, 'Yerres'),
    (9, 'Avenue Georges-Risler', 59250, 'Halluin'),
    (67, 'Rue Émile-Blémont', 17670, 'La Couarde-sur-Mer'),
    (33, 'Boulevard Gallieni', 37600, 'Verneuil-sur-Indre'),
    (98, 'Rue Garancière', 34740, 'Vendargues'),
    (80, 'Square Georges-Contenot', 35140, 'Vendel'),
    (89, 'Impasse des Carrières', 17260, 'Virollet'),
    (35, 'Impasse des Jardiniers', 47350, 'Seyches'),
    (52, 'Avenue du Général-Dodds', 45220, 'Triguères'),
    (55, 'Avenue du Docteur-Gley', 84380, 'Mazan'),
    (43, 'Rue Baudoin', 16290, 'Saint-Saturnin'),
    (78, 'Rue du Grenier-Saint-Lazare', 73150, 'Val-d''Isère'),
    (27, 'Rue Gerbier', 46350, 'Reilhaguet'),
    (57, 'Avenue Dutuit', 13950, 'Cadolive'),
    (75, 'Rue Castex', 29720, 'Tréogat'),
    (32, 'Quai de la Gare', 12850, 'Sainte-Radegonde'),
    (75, 'Rue Henri-Brisson', 73400, 'Ugine'),
    (96, 'Place Édouard-Renard', 36120, 'Sassierges-Saint-Germain'),
    (86, 'Rue Hautefeuille', 12380, 'Saint-Sernin-sur-Rance'),
    (42, 'Port de Grenelle', 35610, 'Vieux-Viel'),
    (44, 'Rue Gramme', 91290, 'Ollainville'),
    (15, 'Rue Humblot', 33540, 'Sauveterre-de-Guyenne'),
    (59, 'Boulevard Henri-IV', 13250, 'Saint-Chamas'),
    (24, 'Villa Belliard', 20214, 'Montemaggiore'),
    (9, 'Rue Étienne-Marcel', 13440, 'Cabannes'),
    (1, 'Rue Herran', 69470, 'Cours-La-Ville'),
    (47, 'Cité Fénelon', 04640, 'Villars-Colmars'),
    (41, 'Cité des Écoles', 49110, 'Saint-Rémy-en-Mauges'),
    (42, 'Rue Cavé', 77840, 'Germigny-sous-Coulombs'),
    (88, 'Rue de l''Amiral-Hamelin', 14520, 'Sainte-Honorine-des-Pertes'),
    (89, 'Avenue Beaucour', 41360, 'Savigny-sur-Braye'),
    (39, 'Rue Eugène-Carrière', 89130, 'Villiers-Saint-Benoît'),
    (61, 'Rue Fantin-Latour', 39380, 'La Vieille-Loye'),
    (76, 'Rue Guisarde', 29600, 'Sainte-Sève'),
    (28, 'Rue Henri-Murger', 59157, 'Fontaine-au-Pire'),
    (37, 'Sente des Dorées', 43380, 'Villeneuve-d''Allier'),
    (61, 'Rue Geoffroy-Saint-Hilaire', 12540, 'Saint-Beaulize'),
    (14, 'Impasse Gomboust', 16410, 'Vouzan'),
    (71, 'Impasse Érard', 42122, 'Saint-Marcel-de-Félines'),
    (160, 'Avenue Hoche', 29980,'Île-Tudy'),
    (236, 'Passage Gustave-Lepeu', 64120,'Uhart-Mixe'),
    (64, 'Rue de Hesse', 44100,'Nantes')
;


INSERT INTO public.compte_client
    (compte_id, adresse_id, use_adresse)
VALUES
    (16, 16, 'Domicile'),
    (16, 17, 'Facturation'),
    (17, 18, 'Domicile/Facturation'),
    (18, 19, 'Domicile/Facturation'),
    (19, 20, 'Domicile/Facturation'),
    (20, 21, 'Domicile/Facturation'),
    (21, 22, 'Domicile/Facturation'),
    (22, 23, 'Domicile/Facturation'),
    (23, 24, 'Domicile/Facturation'),
    (24, 25, 'Domicile'),
    (24, 26, 'Facturation'), 
    (25, 27, 'Domicile/Facturation'),
    (26, 28, 'Domicile/Facturation'),
    (27, 29, 'Domicile/Facturation'),
    (28, 30, 'Domicile/Facturation'),
    (29, 31, 'Domicile'),
    (29, 32, 'Facturation'),
    (30, 33, 'Domicile/Facturation'),
    (31, 34, 'Domicile/Facturation'),
    (32, 35, 'Domicile/Facturation'),
    (33, 36, 'Domicile/Facturation'),
    (34, 37, 'Domicile/Facturation'),
    (35, 38, 'Domicile/Facturation'),
    (36, 39, 'Domicile/Facturation'),
    (37, 40, 'Domicile/Facturation'),
    (38, 41, 'Domicile/Facturation'),
    (39, 42, 'Domicile/Facturation'),
    (40, 43, 'Domicile/Facturation'),
    (41, 44, 'Domicile'),
    (41, 45, 'Facturation'),
    (42, 46, 'Domicile/Facturation'),
    (43, 47, 'Domicile/Facturation'),
    (44, 48, 'Domicile/Facturation'),
    (45, 49, 'Domicile/Facturation'),
    (46, 50, 'Domicile/Facturation'),
    (47, 56, 'Domicile/Facturation'),
    (48, 57, 'Domicile/Facturation'),
    (49, 58, 'Domicile/Facturation')
;


INSERT INTO public.restaurant
    (nom, adresse_id, telephone)
VALUES 
    ('Pizzeria Henri-Murger', 51, '02.08.68.19.02'),
    ('Pizzeria Sente des Dorées', 52, '02.31.62.13.97'),
    ('Pizzeria Geoffroy-Saint-Hilaire', 53, '02.34.96.80.53'),
    ('Pizzeria Impasse Gomboust', 54, '02.23.61.54.37' ),
    ('Pizzeria Impasse Érard', 55, '02.23.40.85.84')
;


INSERT INTO public.compte_employe
    (compte_id, poste, adresse_id, numero_secu, restaurant_id)
VALUES
    (1, 'Cuisinier', 1, 294176465670787, 1),
    (2, 'Cuisinier', 2, 254652187686212, 2),
    (3, 'Cuisinier', 3, 136492516725617, 3),
    (4, 'Cuisinier', 4, 125833906589877, 4),
    (5, 'Cuisinier', 5, 181523774080625, 5),
    (6, 'Livreur', 6, 270571173199025, 1),
    (7, 'Livreur', 7, 108078012089573, 2),
    (8, 'Livreur', 8, 114063736627757, 3),
    (9, 'Livreur', 9, 174423232895627, 4),
    (10, 'Livreur', 10, 114948384751798, 5),
    (11, 'Hote/Hotesse', 11, 142301582544169, 1),
    (12, 'Hote/Hotesse', 12, 117469296149877, 2),
    (13, 'Hote/Hotesse', 13, 286552026685944, 3),
    (14, 'Hote/Hotesse', 14, 255053187884276, 4),
    (15, 'Hote/Hotesse', 15, 221882265634048, 5)
;


INSERT  INTO public.reclamation 
    (compte_client_id, objet, reclamation, restaurant_id)
VALUES
    (2, 'Retard', 'Vehicula est hendrerit est aliquam risus facilisis sapien. amet quis aliquam neque potenti scelerisque. curabitur sodales aenean nullam Urna nisl donec semper. varius dictumst conubia augue. vehicula lectus pellentesque nunc adipiscing et commodo.', 2),
    (22, 'Produit manquant', 'Dictumst curabitur at quis quisque suspendisse. sodales sollicitudin commodo nullam cursus sem. eros Cubilia bibendum nibh quisque. sociosqu potenti vulputate magna gravida arcu lacinia.', 5)
;


INSERT INTO public.note
    (compte_client_id, note, commentaire)
VALUES
    (1, '5/5', 'Rutrum sociosqu potenti velit. dui euismod habitasse est condimentum. vitae varius fusce neque arcu at. interdum platea ad commodo. suspendisse Pretium et phasellus bibendum hac. primis in urna mauris ut blandit rutrum.'),
    (4, '4/5', 'Sed varius sed per pellentesque. mollis imperdiet vel diam. magna elementum curabitur elementum magna massa elit Faucibus purus sollicitudin nam. odio velit ut augue tortor fringilla sem ultrices. vel sit lectus adipiscing orci molestie maecenas. arcu'),
    (20, '3/5', 'Tincidunt felis mattis luctus luctus integer proin. placerat proin feugiat scelerisque Lorem facilisis et ligula non. sociosqu eleifend euismod purus. integer ultricies felis'),
    (12, '5/5', 'Hendrerit ipsum gravida elit dolor imperdiet euismod nisl. morbi lectus fermentum molestie Litora venenatis nunc vehicula nisl ipsum vulputate. dictum massa turpis'),
    (32, '2/5', 'Quam faucibus aliquam semper viverra duis. aptent netus suspendisse ultrices dictumst donec phasellus id. gravida porta arcu eros Gravida lorem viverra himenaeos ut eget. elementum augue congue integer')
;


INSERT INTO public.categorie
    (nom)
VALUES
    ('Boissons'),
    ('Glaces'),
    ('Pizzas')
;


INSERT INTO public.produit
    (nom, descriptif, prix_unitaire, categorie_id)
VALUES
    ('Coca cola', 'Mattis non sociosqu faucibus. fermentum suspendisse ligula vestibulum lacinia sagittis', 2.50, 1),
    ('Orangina', 'Potenti fermentum mauris eu donec. semper aliquam netus cras curabitur risus vehicula. leo nulla et vitae. vivamus pulvinar', 2.50, 1),
    ('Evian','Felis leo lobortis sed fermentum nisl vestibulum. quisque eu taciti', 1.50, 1),
    ('Rosé', 'Luctus sodales risus iaculis hac a suscipit. sed augue curabitur', 6.50, 1),
    ('Magnum chocolat noir', 'Volutpat at sit gravida. malesuada cubilia nisl quisque class. proin justo phasellus tristique', 2.00, 2),
    ('Cornetto café', 'Dui cursus dolor vehicula aliquam. himenaeos odio proin scelerisque. conubia quam', 2.00, 2),
    ('Haagen-dazs macadamia', 'Porttitor risus tincidunt ultrices curabitur auctor. ut ut dictum venenatis adipiscing sagittis est. class', 4.00, 2),
    ('MARGARITA', 'Libero consectetur nec lacus. nam quisque sociosqu dictumst sociosqu. urna egestas', 8.50, 3),
    ('REINE','Sodales cubilia aliquet nunc. iaculis a ornare feugiat taciti lacinia. posuere lectus velit ac. quis metus dui duis nam', 9.50, 3),
    ('4 FROMAGE', 'Auctor hendrerit consectetur habitasse ligula. vitae malesuada sagittis vivamus eros sodales. quis primis dolor sollicitudin nulla. laoreet arcu', 11.00, 3),
    ('PARMIGIANA', 'Primis metus praesent erat sem pharetra venenatis. elit lacinia iaculis vel velit class leo nullam. luctus', 10.50, 3),
    ('4 SAISONS', 'Et facilisis in rutrum himenaeos sit vel etiam. fermentum magna arcu ac fusce. hac libero consequat', 10.50, 3),
    ('SAUMON', 'Neque suscipit faucibus tempor. nisl eleifend elementum lobortis. lacinia non lectus aliquam dictum tristique inceptos. ad', 12.50, 3),
    ('BOLOGNA', 'Aenean leo suspendisse a lorem tempus. mauris volutpat ut adipiscing. elit odio luctus curae ullamcorper suscipit.', 11.50, 3),
    ('CALZONE', 'Vivamus pellentesque quisque mollis sem. rutrum metus ac condimentum habitasse etiam condimentum. auctor sit porta arcu rutrum sollicitudin', 10.00, 3),
    ('VEGETARIENNE', 'Pretium nostra porttitor condimentum ultricies suscipit iaculis. tortor tortor placerat volutpat orci donec.', 12.00, 3),
    ('JAMBON CRU', 'Blandit suspendisse class justo aliquet auctor aenean donec. potenti ligula himenaeos lectus', 13.00, 3)
;


INSERT INTO public.ingredient
    (ingredient)
VALUES
    ('Jambon'),
    ('Champignon de paris'),
    ('Souce tomate'),
    ('Emmental'),
    ('Mozzarella'),
    ('Basilic'),
    ('Huile d’olive'),
    ('Origan'),
    ('Anchois'),
    ('Artichauts'),
    ('Olive provencale'),
    ('Gorgonzola'),
    ('Parmesan'),
    ('Fontina'),
    ('Cârpes'),
    ('Crème'),
    ('Oeuf'),
    ('Aubergine'),
    ('Boeuf haché'),
    ('Oignon'),
    ('Saumon'),
    ('Ricotta'),
    ('Aneth'),
    ('Chèvre'),
    ('Jambon cru'),
    ('Tomate')
;


INSERT INTO public.recette
    (nom, recette)
VALUES 
    ('MARGARITA', 'Vitae tortor pharetra euismod ultricies purus. egestas ullamcorper duis curabitur volutpat lacus ullamcorper. maecenasSollicitudin proin ad urna lobortis imperdiet ante. orci dui donec semper quis at sociosqu tristique. ipsum Amet sagittis laoreet facilisis. torquent etiam nec eros condimentum nisiVestibulum eleifend condimentum magna taciti. euismod purus velit augue nullam diam. arcu amet integer aptent consectetur litora. et sagittis Id vitae proin donec vitae eros. magna dolor habitasse litora lacus euismod platea. ultricies habitasse leo scelerisque. hac imperdietAuctor laoreet molestie aenean proin himenaeos at. luctus enim eleifend'),
   
    ('REINE', 'Vitae tortor pharetra euismod ultricies purus. egestas ullamcorper duis curabitur volutpat lacus ullamcorper. maecenasSollicitudin proin ad urna lobortis imperdiet ante. orci dui donec semper quis at sociosqu tristique. ipsum Amet sagittis laoreet facilisis. torquent etiam nec eros condimentum nisiVestibulum eleifend condimentum magna taciti. euismod purus velit augue nullam diam. arcu amet integer aptent consectetur litora. et sagittis Id vitae proin donec vitae eros. magna dolor habitasse litora lacus euismod platea. ultricies habitasse leo scelerisque. hac imperdietAuctor laoreet molestie aenean proin himenaeos at. luctus enim eleifend'),
   
    ('4 FROMAGE', 'Vitae tortor pharetra euismod ultricies purus. egestas ullamcorper duis curabitur volutpat lacus ullamcorper. maecenasSollicitudin proin ad urna lobortis imperdiet ante. orci dui donec semper quis at sociosqu tristique. ipsum Amet sagittis laoreet facilisis. torquent etiam nec eros condimentum nisiVestibulum eleifend condimentum magna taciti. euismod purus velit augue nullam diam. arcu amet integer aptent consectetur litora. et sagittis Id vitae proin donec vitae eros. magna dolor habitasse litora lacus euismod platea. ultricies habitasse leo scelerisque. hac imperdietAuctor laoreet molestie aenean proin himenaeos at. luctus enim eleifend'),
    
    ('PARMIGIANA', 'Vitae tortor pharetra euismod ultricies purus. egestas ullamcorper duis curabitur volutpat lacus ullamcorper. maecenasSollicitudin proin ad urna lobortis imperdiet ante. orci dui donec semper quis at sociosqu tristique. ipsum Amet sagittis laoreet facilisis. torquent etiam nec eros condimentum nisiVestibulum eleifend condimentum magna taciti. euismod purus velit augue nullam diam. arcu amet integer aptent consectetur litora. et sagittis Id vitae proin donec vitae eros. magna dolor habitasse litora lacus euismod platea. ultricies habitasse leo scelerisque. hac imperdietAuctor laoreet molestie aenean proin himenaeos at. luctus enim eleifend'),
    
    ('4 SAISONS', 'Vitae tortor pharetra euismod ultricies purus. egestas ullamcorper duis curabitur volutpat lacus ullamcorper. maecenasSollicitudin proin ad urna lobortis imperdiet ante. orci dui donec semper quis at sociosqu tristique. ipsum Amet sagittis laoreet facilisis. torquent etiam nec eros condimentum nisiVestibulum eleifend condimentum magna taciti. euismod purus velit augue nullam diam. arcu amet integer aptent consectetur litora. et sagittis Id vitae proin donec vitae eros. magna dolor habitasse litora lacus euismod platea. ultricies habitasse leo scelerisque. hac imperdietAuctor laoreet molestie aenean proin himenaeos at. luctus enim eleifend'),
    
    ('SAUMON', 'Vitae tortor pharetra euismod ultricies purus. egestas ullamcorper duis curabitur volutpat lacus ullamcorper. maecenasSollicitudin proin ad urna lobortis imperdiet ante. orci dui donec semper quis at sociosqu tristique. ipsum Amet sagittis laoreet facilisis. torquent etiam nec eros condimentum nisiVestibulum eleifend condimentum magna taciti. euismod purus velit augue nullam diam. arcu amet integer aptent consectetur litora. et sagittis Id vitae proin donec vitae eros. magna dolor habitasse litora lacus euismod platea. ultricies habitasse leo scelerisque. hac imperdietAuctor laoreet molestie aenean proin himenaeos at. luctus enim eleifend'),
    
    ('BOLOGNA', 'Vitae tortor pharetra euismod ultricies purus. egestas ullamcorper duis curabitur volutpat lacus ullamcorper. maecenasSollicitudin proin ad urna lobortis imperdiet ante. orci dui donec semper quis at sociosqu tristique. ipsum Amet sagittis laoreet facilisis. torquent etiam nec eros condimentum nisiVestibulum eleifend condimentum magna taciti. euismod purus velit augue nullam diam. arcu amet integer aptent consectetur litora. et sagittis Id vitae proin donec vitae eros. magna dolor habitasse litora lacus euismod platea. ultricies habitasse leo scelerisque. hac imperdietAuctor laoreet molestie aenean proin himenaeos at. luctus enim eleifend'),
    
    ('CALZONE', 'Vitae tortor pharetra euismod ultricies purus. egestas ullamcorper duis curabitur volutpat lacus ullamcorper. maecenasSollicitudin proin ad urna lobortis imperdiet ante. orci dui donec semper quis at sociosqu tristique. ipsum Amet sagittis laoreet facilisis. torquent etiam nec eros condimentum nisiVestibulum eleifend condimentum magna taciti. euismod purus velit augue nullam diam. arcu amet integer aptent consectetur litora. et sagittis Id vitae proin donec vitae eros. magna dolor habitasse litora lacus euismod platea. ultricies habitasse leo scelerisque. hac imperdietAuctor laoreet molestie aenean proin himenaeos at. luctus enim eleifend'),
    
    ('Végétarienne', 'Vitae tortor pharetra euismod ultricies purus. egestas ullamcorper duis curabitur volutpat lacus ullamcorper. maecenasSollicitudin proin ad urna lobortis imperdiet ante. orci dui donec semper quis at sociosqu tristique. ipsum Amet sagittis laoreet facilisis. torquent etiam nec eros condimentum nisiVestibulum eleifend condimentum magna taciti. euismod purus velit augue nullam diam. arcu amet integer aptent consectetur litora. et sagittis Id vitae proin donec vitae eros. magna dolor habitasse litora lacus euismod platea. ultricies habitasse leo scelerisque. hac imperdietAuctor laoreet molestie aenean proin himenaeos at. luctus enim eleifend'),
    
    ('Jambon cru', 'Vitae tortor pharetra euismod ultricies purus. egestas ullamcorper duis curabitur volutpat lacus ullamcorper. maecenasSollicitudin proin ad urna lobortis imperdiet ante. orci dui donec semper quis at sociosqu tristique. ipsum Amet sagittis laoreet facilisis. torquent etiam nec eros condimentum nisiVestibulum eleifend condimentum magna taciti. euismod purus velit augue nullam diam. arcu amet integer aptent consectetur litora. et sagittis Id vitae proin donec vitae eros. magna dolor habitasse litora lacus euismod platea. ultricies habitasse leo scelerisque. hac imperdietAuctor laoreet molestie aenean proin himenaeos at. luctus enim eleifend')
;

   
INSERT INTO public.ingredient_recette
    (recette_id, ingredient_id, quantite)
VALUES
    (1, 26, '50g'),
    (1, 6, '100g'),
    (1, 8, '20g'),
    (1, 3, '50g'),
    
    (2, 26, '50g'),
    (2, 6, '100g'),
    (2, 1, '150g'),
    (2, 13, '10g'),
    (2, 9, '5g'),
    (2, 3, '40g'),
    (2, 2, '50g'),
    (2, 11, '10g'),
    
    (3, 25, '50g'),
    (3, 5, '50g'),
    (3, 13, '50g'),
    (3, 14, '50g'),
    
    (4, 3, '50g'),
    (4, 26, '50g'),
    (4, 19, '70g'),
    (4, 6, '50g'),
    (4, 13, '30g'),
    
    (5, 3, '50g'),
    (5, 10, '100g'),
    (5, 13, '30g'),
    (5, 1, '100g'),
    (5, 26, '50g'),
    
    (6, 24, '150g'),
    (6, 22, '10g'),
    (6, 26, '50g'),
    (6, 4, '50g'),
    (6, 17, '30g'),
    
    (7, 3, '50g'),
    (7, 26, '50g'),
    (7, 8, '50g'),
    (7, 20, '30g'),
    (7, 7, '10g'),
    (7, 21, '150g'),
    (7, 18, '1'),
    
    (8, 2, '50g'),
    (8, 6, '50g'),
    (8, 1, '150g'),
    (8, 3, '40g'),
    (8, 16, '20ml'),
    (8, 18, '1'),
    
    (9, 3, '50g'),
    (9, 11, '50g'),
    (9, 19, '50g'),
    (9, 2, '10g'),
    (9, 8, '5cl'),
    (9, 6, '10g'),
    
    (10, 3, '50g'),
    (10, 26, '150g'),
    (10, 6, '50g'),
    (10, 2, '10g'),
    (10, 8, '5cl'),
    (10, 14, '30g')
;
    
    
INSERT INTO public.ingredient_produit
    (produit_id, ingredient_id, quantite)
VALUES
    (8, 26, '50g'),
    (8, 6, '100g'),
    (8, 8, '20g'),
    (8, 3, '50g'),
    
    (9, 26, '50g'),
    (9, 6, '100g'),
    (9, 1, '150g'),
    (9, 13, '10g'),
    (9, 9, '5g'),
    (9, 3, '40g'),
    (9, 2, '50g'),
    (9, 11, '10g'),
    
    (10, 25, '50g'),
    (10, 5, '50g'),
    (10, 13, '50g'),
    (10, 14, '50g'),
    
    (11, 3, '50g'),
    (11, 26, '50g'),
    (11, 19, '70g'),
    (11, 6, '50g'),
    (11, 13, '30g'),
    
    (12, 3, '50g'),
    (12, 10, '100g'),
    (12, 13, '30g'),
    (12, 1, '100g'),
    (12, 26, '50g'),
    
    (13, 24, '150g'),
    (13, 22, '10g'),
    (13, 26, '50g'),
    (13, 4, '50g'),
    (13, 17, '30g'),
    
    (14, 3, '50g'),
    (14, 26, '50g'),
    (14, 8, '50g'),
    (14, 20, '30g'),
    (14, 7, '10g'),
    (14, 21, '150g'),
    (14, 18, '1'),
    
    (15, 2, '50g'),
    (15, 6, '50g'),
    (15, 1, '150g'),
    (15, 3, '40g'),
    (15, 16, '20ml'),
    (15, 18, '1'),
    
    (16, 3, '50g'),
    (16, 11, '50g'),
    (16, 19, '50g'),
    (16, 2, '10g'),
    (16, 8, '5cl'),
    (16, 6, '10g'),
    
    (17, 3, '50g'),
    (17, 26, '150g'),
    (17, 6, '50g'),
    (17, 2, '10g'),
    (17, 8, '5cl'),
    (17, 14, '30g')
;
    
    
INSERT INTO public.stock
    (restaurant_id, ingredient_id, quantite, unite_de_mesure)
VALUES
    (1, 1, 50, 'kg'),  
    (1, 2, 50, 'kg'),
    (1, 3, 10, 'L'),
    (1, 4, 20, 'kg'),    
    (1, 5, 30, 'kg'),    
    (1, 6, 30, 'kg'),    
    (1, 7, 10, 'L'),    
    (1, 8, 30, 'kg'),    
    (1, 9, 10, 'kg'),    
    (1, 10, 10, 'kg'),    
    (1, 11, 15, 'kg'),    
    (1, 12, 13, 'kg'),    
    (1, 13, 10, 'kg'),    
    (1, 14, 20, 'kg'),    
    (1, 16, 15, 'L'),    
    (1, 17, 9, 'Unité'),    
    (1, 18, 5, 'kg'),    
    (1, 19, 50, 'kg'),    
    (1, 20, 21, 'kg'),    
    (1, 21, 23, 'kg'),    
    (1, 22, 6, 'kg'),    
    (1, 24, 7, 'kg'),    
    (1, 25, 20, 'kg'),    
    (1, 26, 15, 'kg'),    
   
   
    (2, 1, 50, 'kg'),  
    (2, 2, 50, 'kg'),
    (2, 3, 10, 'L'),
    (2, 4, 20, 'kg'),    
    (2, 5, 30, 'kg'),    
    (2, 6, 30, 'kg'),    
    (2, 7, 10, 'L'),    
    (2, 8, 30, 'kg'),    
    (2, 9, 10, 'kg'),    
    (2, 10, 10, 'kg'),    
    (2, 11, 15, 'kg'),    
    (2, 12, 13, 'kg'),    
    (2, 13, 10, 'kg'),    
    (2, 14, 20, 'kg'),    
    (2, 16, 15, 'L'),    
    (2, 17, 9, 'Unité'),    
    (2, 18, 5, 'kg'),    
    (2, 19, 50, 'kg'),    
    (2, 20, 21, 'kg'),    
    (2, 21, 23, 'kg'),    
    (2, 22, 6, 'kg'),    
    (2, 24, 7, 'kg'),    
    (2, 25, 20, 'kg'),    
    (2, 26, 15, 'kg'),    
       
    
    (3, 1, 50, 'kg'),  
    (3, 2, 50, 'kg'),
    (3, 3, 10, 'L'),
    (3, 4, 20, 'kg'),    
    (3, 5, 30, 'kg'),    
    (3, 6, 30, 'kg'),    
    (3, 7, 10, 'L'),    
    (3, 8, 30, 'kg'),    
    (3, 9, 10, 'kg'),    
    (3, 10, 10, 'kg'),    
    (3, 11, 15, 'kg'),    
    (3, 12, 13, 'kg'),    
    (3, 13, 10, 'kg'),    
    (3, 14, 20, 'kg'),    
    (3, 16, 15, 'L'),    
    (3, 17, 9, 'Unité'),    
    (3, 18, 5, 'kg'),    
    (3, 19, 50, 'kg'),    
    (3, 20, 21, 'kg'),    
    (3, 21, 23, 'kg'),    
    (3, 22, 6, 'kg'),    
    (3, 24, 7, 'kg'),    
    (3, 25, 20, 'kg'),    
    (3, 26, 15, 'kg'),    
      
    
    (4, 1, 50, 'kg'),  
    (4, 2, 50, 'kg'),
    (4, 3, 10, 'L'),
    (4, 4, 20, 'kg'),    
    (4, 5, 30, 'kg'),    
    (4, 6, 30, 'kg'),    
    (4, 7, 10, 'L'),    
    (4, 8, 30, 'kg'),    
    (4, 9, 10, 'kg'),    
    (4, 10, 10, 'kg'),    
    (4, 11, 15, 'kg'),    
    (4, 12, 13, 'kg'),    
    (4, 13, 10, 'kg'),    
    (4, 14, 20, 'kg'),    
    (4, 16, 15, 'L'),    
    (4, 17, 9, 'Unité'),    
    (4, 18, 5, 'kg'),    
    (4, 19, 50, 'kg'),    
    (4, 20, 21, 'kg'),    
    (4, 21, 23, 'kg'),    
    (4, 22, 6, 'kg'),    
    (4, 24, 7, 'kg'),    
    (4, 25, 20, 'kg'),    
    (4, 26, 15, 'kg'),    
   

    (5, 1, 50, 'kg'),  
    (5, 2, 50, 'kg'),
    (5, 3, 10, 'L'),
    (5, 4, 20, 'kg'),    
    (5, 5, 30, 'kg'),    
    (5, 6, 30, 'kg'),    
    (5, 7, 10, 'L'),    
    (5, 8, 30, 'kg'),    
    (5, 9, 10, 'kg'),    
    (5, 10, 10, 'kg'),    
    (5, 11, 15, 'kg'),    
    (5, 12, 13, 'kg'),    
    (5, 13, 10, 'kg'),    
    (5, 14, 20, 'kg'),    
    (5, 16, 15, 'L'),    
    (5, 17, 9, 'Unité'),    
    (5, 18, 5, 'kg'),    
    (5, 19, 50, 'kg'),    
    (5, 20, 21, 'kg'),    
    (5, 21, 23, 'kg'),    
    (5, 22, 6, 'kg'),    
    (5, 24, 7, 'kg'),    
    (5, 25, 20, 'kg'),    
    (5, 26, 15, 'kg')    
        
;


INSERT INTO public.commande
    (numero_de_commande, date, statut, restaurant_id, compte_client_id, compte_employe_id)
VALUES
    (7750945, '2017-10-03', 'Commande en cour préparation', 1, 22, null), 
    (423447, '2017-08-20', 'Commande en cour livraison', 1, 3, null), 
    (1626256, '2017-08-17', 'Commande en cour préparation', 2, 22, null), 
    (7107092, '2017-12-24', 'Commande en attente de préparation', 2, null ,15),
    (8629753, '2017-09-17', 'Commande en cour préparation', 3, 10, null), 
    (3999371, '2017-06-26', 'Commande livrée', 3, 6, null), 
    (385370, '2017-06-14', 'Commande livrée', 4, 24, null), 
    (9207303, '2017-10-17', 'Commande en attente de préparation', 4, 2, null), 
    (9400389, '2017-03-02', 'Commande en cour livraison', 5, 4, null), 
    (9226950, '2017-11-12', 'Commande livrée', 5, null , 11)
;


INSERT INTO public.ligne_de_commande
    (numero_de_commande, produit_id, quantite)
VALUES
    (7750945, 1, 2),
    (7750945, 10, 1),
    (7750945, 17, 1),
    (7750945, 7, 1),
    
    (423447, 3, 1),
    (423447, 13, 1),
    
    (1626256, 4, 1),
    (1626256, 9, 2),
    (1626256, 6, 2),
    
    (7107092, 11, 1),
    
    (8629753, 12, 1),
    (8629753, 15, 1),
    (8629753, 2, 1),
    (8629753, 1, 1),
    (8629753, 5, 2),
    
    (3999371, 14, 1),
    (3999371, 6, 1),
    (3999371, 2, 1),
    
    (385370, 11, 1),
    (385370, 16, 1),
    
    (9207303, 8, 1),
    (9207303, 3, 1),
    
    (9400389, 15, 2),
    (9400389, 5, 2),
    (9400389, 2, 2),
    
    (9226950, 12, 1)
;


INSERT INTO public.facture
    (numero_de_facture, numero_de_commande, statut)
VALUES 
    (55213, 7750945,'A régler'),
    (213655, 423447, 'Régler'),
    (124569, 1626256, 'Régler'),
    (123456, 7107092, 'Régler'),
    (1254, 8629753, 'A régler')
;
    
































    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    



