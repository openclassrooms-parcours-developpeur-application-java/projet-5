
CREATE SEQUENCE public.categorie_id_seq;

CREATE TABLE public.categorie (
                id INTEGER NOT NULL DEFAULT nextval('public.categorie_id_seq'),
                nom VARCHAR(50) NOT NULL,
                CONSTRAINT categorie_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.categorie_id_seq OWNED BY public.categorie.id;

CREATE SEQUENCE public.produit_id_seq;

CREATE TABLE public.produit (
                id INTEGER NOT NULL DEFAULT nextval('public.produit_id_seq'),
                nom VARCHAR(50) NOT NULL,
                descriptif VARCHAR NOT NULL,
                prix_unitaire DOUBLE PRECISION NOT NULL,
                categorie_id INTEGER NOT NULL,
                CONSTRAINT produit_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.produit_id_seq OWNED BY public.produit.id;

CREATE SEQUENCE public.recette_id_seq;

CREATE TABLE public.recette (
                id INTEGER NOT NULL DEFAULT nextval('public.recette_id_seq'),
                nom VARCHAR(50) NOT NULL,
                recette VARCHAR NOT NULL,
                CONSTRAINT recette_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.recette_id_seq OWNED BY public.recette.id;

CREATE SEQUENCE public.ingredient_id_seq;

CREATE TABLE public.ingredient (
                id INTEGER NOT NULL DEFAULT nextval('public.ingredient_id_seq'),
                ingredient VARCHAR NOT NULL,
                CONSTRAINT ingredient_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.ingredient_id_seq OWNED BY public.ingredient.id;

CREATE TABLE public.ingredient_produit (
                ingredient_id INTEGER NOT NULL,
                produit_id INTEGER NOT NULL,
                quantite VARCHAR NOT NULL,
                CONSTRAINT ingredient_produit_pk PRIMARY KEY (ingredient_id, produit_id)
);


CREATE TABLE public.ingredient_recette (
                ingredient_id INTEGER NOT NULL,
                recette_id INTEGER NOT NULL,
                quantite VARCHAR NOT NULL,
                CONSTRAINT ingredient_recette_pk PRIMARY KEY (ingredient_id, recette_id)
);


CREATE SEQUENCE public.adresse_id_seq;

CREATE TABLE public.adresse (
                id INTEGER NOT NULL DEFAULT nextval('public.adresse_id_seq'),
                numero INTEGER NOT NULL,
                rue VARCHAR NOT NULL,
                code_postale INTEGER NOT NULL,
                localite VARCHAR NOT NULL,
                CONSTRAINT adresse_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.adresse_id_seq OWNED BY public.adresse.id;

CREATE SEQUENCE public.restaurant_id_seq;

CREATE TABLE public.restaurant (
                id INTEGER NOT NULL DEFAULT nextval('public.restaurant_id_seq'),
                nom VARCHAR NOT NULL,
                telephone VARCHAR NOT NULL,
                adresse_id INTEGER NOT NULL,
                CONSTRAINT restaurant_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.restaurant_id_seq OWNED BY public.restaurant.id;

CREATE TABLE public.stock (
                restaurant_id INTEGER NOT NULL,
                ingredient_id INTEGER NOT NULL,
                unite_de_mesure VARCHAR NOT NULL,
                quantite INTEGER NOT NULL,
                CONSTRAINT stock_pk PRIMARY KEY (restaurant_id, ingredient_id)
);


CREATE SEQUENCE public.compte_id_seq;

CREATE TABLE public.compte (
                id INTEGER NOT NULL DEFAULT nextval('public.compte_id_seq'),
                nom VARCHAR(50) NOT NULL,
                prenom VARCHAR(50) NOT NULL,
                mot_de_passe VARCHAR(20) NOT NULL,
                mail VARCHAR(50) NOT NULL,
                telephone VARCHAR NOT NULL,
                CONSTRAINT compte_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.compte_id_seq OWNED BY public.compte.id;

CREATE SEQUENCE public.compte_client_id_seq;

CREATE TABLE public.compte_client (
                id INTEGER NOT NULL DEFAULT nextval('public.compte_client_id_seq'),
                compte_id INTEGER NOT NULL,
                use_adresse VARCHAR NOT NULL,
                adresse_id INTEGER NOT NULL,
                CONSTRAINT compte_client_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.compte_client_id_seq OWNED BY public.compte_client.id;

CREATE TABLE public.reclamation (
                compte_client_id INTEGER NOT NULL,
                objet VARCHAR(50) NOT NULL,
                reclamation VARCHAR(1000) NOT NULL,
                restaurant_id INTEGER NOT NULL,
                CONSTRAINT reclamation_pk PRIMARY KEY (compte_client_id)
);


CREATE TABLE public.note (
                compte_client_id INTEGER NOT NULL,
                note VARCHAR NOT NULL,
                commentaire VARCHAR(500),
                CONSTRAINT note_pk PRIMARY KEY (compte_client_id)
);


CREATE TABLE public.compte_employe (
                compte_id INTEGER NOT NULL,
                poste VARCHAR(50) NOT NULL,
                adresse_id INTEGER NOT NULL,
                numero_secu BIGINT NOT NULL,
                restaurant_id INTEGER NOT NULL,
                CONSTRAINT compte_employe_pk PRIMARY KEY (compte_id)
);


CREATE UNIQUE INDEX compte_employe_idx
 ON public.compte_employe
 ( numero_secu );

CLUSTER compte_employe_idx ON compte_employe;

CREATE SEQUENCE public.commande_numero_seq;

CREATE TABLE public.commande (
                numero_de_commande INTEGER NOT NULL DEFAULT nextval('public.commande_numero_seq'),
                date DATE NOT NULL,
                statut VARCHAR(50) NOT NULL,
                restaurant_id INTEGER NOT NULL,
                compte_client_id INTEGER,
                compte_employe_id INTEGER,
                CONSTRAINT commande_pk PRIMARY KEY (numero_de_commande)
);


ALTER SEQUENCE public.commande_numero_seq OWNED BY public.commande.numero_de_commande;

CREATE TABLE public.ligne_de_commande (
                produit_id INTEGER NOT NULL,
                numero_de_commande INTEGER NOT NULL,
                quantite INTEGER NOT NULL,
                CONSTRAINT ligne_de_commande_pk PRIMARY KEY (produit_id, numero_de_commande)
);


CREATE SEQUENCE public.facture_numero_seq;

CREATE TABLE public.facture (
                numero_de_facture INTEGER NOT NULL DEFAULT nextval('public.facture_numero_seq'),
                numero_de_commande INTEGER NOT NULL,
                statut VARCHAR(50) NOT NULL,
                CONSTRAINT facture_pk PRIMARY KEY (numero_de_facture)
);


ALTER SEQUENCE public.facture_numero_seq OWNED BY public.facture.numero_de_facture;

ALTER TABLE public.produit ADD CONSTRAINT categorie_produit_fk
FOREIGN KEY (categorie_id)
REFERENCES public.categorie (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.ligne_de_commande ADD CONSTRAINT produit_ligne_de_commande_fk
FOREIGN KEY (produit_id)
REFERENCES public.produit (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.ingredient_produit ADD CONSTRAINT produit_ingredient_produit_fk
FOREIGN KEY (produit_id)
REFERENCES public.produit (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.ingredient_recette ADD CONSTRAINT recette_ingredient_recette_fk
FOREIGN KEY (recette_id)
REFERENCES public.recette (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.stock ADD CONSTRAINT ingredient_stock_fk
FOREIGN KEY (ingredient_id)
REFERENCES public.ingredient (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.ingredient_recette ADD CONSTRAINT ingredient_ingredient_recette_fk
FOREIGN KEY (ingredient_id)
REFERENCES public.ingredient (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.ingredient_produit ADD CONSTRAINT ingredient_ingredient_produit_fk
FOREIGN KEY (ingredient_id)
REFERENCES public.ingredient (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.compte_employe ADD CONSTRAINT adresse_compte_employe_fk
FOREIGN KEY (adresse_id)
REFERENCES public.adresse (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.restaurant ADD CONSTRAINT adresse_restaurant_fk
FOREIGN KEY (adresse_id)
REFERENCES public.adresse (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.compte_client ADD CONSTRAINT adresse_compte_client_fk
FOREIGN KEY (adresse_id)
REFERENCES public.adresse (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.commande ADD CONSTRAINT restaurant_commande_fk
FOREIGN KEY (restaurant_id)
REFERENCES public.restaurant (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.compte_employe ADD CONSTRAINT restaurant_compte_employe_fk
FOREIGN KEY (restaurant_id)
REFERENCES public.restaurant (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.stock ADD CONSTRAINT restaurant_stock_fk
FOREIGN KEY (restaurant_id)
REFERENCES public.restaurant (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.reclamation ADD CONSTRAINT restaurant_reclamation_fk
FOREIGN KEY (restaurant_id)
REFERENCES public.restaurant (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.compte_employe ADD CONSTRAINT compte_compte_employe_fk
FOREIGN KEY (compte_id)
REFERENCES public.compte (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.compte_client ADD CONSTRAINT compte_compte_client_fk
FOREIGN KEY (compte_id)
REFERENCES public.compte (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.note ADD CONSTRAINT compte_client_note_fk
FOREIGN KEY (compte_client_id)
REFERENCES public.compte_client (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.reclamation ADD CONSTRAINT compte_client_reclamation_fk
FOREIGN KEY (compte_client_id)
REFERENCES public.compte_client (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.commande ADD CONSTRAINT compte_client_commande_fk
FOREIGN KEY (compte_client_id)
REFERENCES public.compte_client (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.commande ADD CONSTRAINT compte_employe_commande_fk
FOREIGN KEY (compte_employe_id)
REFERENCES public.compte_employe (compte_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.facture ADD CONSTRAINT commande_facture_fk
FOREIGN KEY (numero_de_commande)
REFERENCES public.commande (numero_de_commande)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.ligne_de_commande ADD CONSTRAINT commande_ligne_de_commande_fk
FOREIGN KEY (numero_de_commande)
REFERENCES public.commande (numero_de_commande)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
